package com.yaruan.utils;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class HibernateUtils {
	
	private static  SessionFactory SessionFactory =HibernateUtils.createSessionFactory();
	
	public static SessionFactory createSessionFactory() {
		
		//默认加载hibernate.cfg.xml配置文件
		Configuration config=new Configuration().configure();
		
		//构建会话工程
		SessionFactory SessionFactory = config.buildSessionFactory();
		
		return SessionFactory;
		
	}
	
	public static Session getSession() {
		Session session = SessionFactory.openSession();
		return session;
	}
	
	
/*	public static void main(String[] args) {
		System.out.println(HibernateUtils.getSession());
	}*/

}
