package com.yaruan.pojo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="cst_customer")
public class Customer {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id")
	private Long CusId;
	
	@Column(name="cst_name")
	private String name;
	@Column(name="cst_age")
	private int age;
	@Column(name="cst_sex")
	private String sex;

	public Long getCusId() {
		return CusId;
	}

	public void setCusId(Long cusId) {
		CusId = cusId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public Customer(Long cusId, String name, int age, String sex) {
		super();
		CusId = cusId;
		this.name = name;
		this.age = age;
		this.sex = sex;
	}

	public Customer() {
		super();
	}

	@Override
	public String toString() {
		return "Customer [CusId=" + CusId + ", name=" + name + ", age=" + age + ", sex=" + sex + "]";
	}

}
